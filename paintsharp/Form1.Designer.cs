﻿
namespace paintsharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbPlatno = new System.Windows.Forms.PictureBox();
            this.lbPovijest = new System.Windows.Forms.ListBox();
            this.gbKistovi = new System.Windows.Forms.GroupBox();
            this.rbKvadrat = new System.Windows.Forms.RadioButton();
            this.rbKruznica = new System.Windows.Forms.RadioButton();
            this.rbTocka = new System.Windows.Forms.RadioButton();
            this.gbPostavkeKista = new System.Windows.Forms.GroupBox();
            this.rbKist3px = new System.Windows.Forms.RadioButton();
            this.rbKist2px = new System.Windows.Forms.RadioButton();
            this.rbKist1px = new System.Windows.Forms.RadioButton();
            this.gbUndoRedo = new System.Windows.Forms.GroupBox();
            this.btnRedo = new System.Windows.Forms.Button();
            this.btnUndo = new System.Windows.Forms.Button();
            this.cbColor = new System.Windows.Forms.CheckBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlatno)).BeginInit();
            this.gbKistovi.SuspendLayout();
            this.gbPostavkeKista.SuspendLayout();
            this.gbUndoRedo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbPlatno
            // 
            this.pbPlatno.Location = new System.Drawing.Point(0, 0);
            this.pbPlatno.Name = "pbPlatno";
            this.pbPlatno.Size = new System.Drawing.Size(597, 426);
            this.pbPlatno.TabIndex = 0;
            this.pbPlatno.TabStop = false;
            this.pbPlatno.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pbPlatno_MouseDown);
            this.pbPlatno.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pbPlatno_MouseUp);
            // 
            // lbPovijest
            // 
            this.lbPovijest.FormattingEnabled = true;
            this.lbPovijest.ItemHeight = 16;
            this.lbPovijest.Location = new System.Drawing.Point(603, 0);
            this.lbPovijest.Name = "lbPovijest";
            this.lbPovijest.Size = new System.Drawing.Size(305, 532);
            this.lbPovijest.TabIndex = 1;
            // 
            // gbKistovi
            // 
            this.gbKistovi.Controls.Add(this.rbKvadrat);
            this.gbKistovi.Controls.Add(this.rbKruznica);
            this.gbKistovi.Controls.Add(this.rbTocka);
            this.gbKistovi.Location = new System.Drawing.Point(0, 453);
            this.gbKistovi.Name = "gbKistovi";
            this.gbKistovi.Size = new System.Drawing.Size(242, 74);
            this.gbKistovi.TabIndex = 2;
            this.gbKistovi.TabStop = false;
            this.gbKistovi.Text = "Kistovi";
            // 
            // rbKvadrat
            // 
            this.rbKvadrat.AutoSize = true;
            this.rbKvadrat.Location = new System.Drawing.Point(165, 32);
            this.rbKvadrat.Name = "rbKvadrat";
            this.rbKvadrat.Size = new System.Drawing.Size(78, 21);
            this.rbKvadrat.TabIndex = 2;
            this.rbKvadrat.TabStop = true;
            this.rbKvadrat.Text = "Kvadrat";
            this.rbKvadrat.UseVisualStyleBackColor = true;
            // 
            // rbKruznica
            // 
            this.rbKruznica.AutoSize = true;
            this.rbKruznica.Location = new System.Drawing.Point(74, 32);
            this.rbKruznica.Name = "rbKruznica";
            this.rbKruznica.Size = new System.Drawing.Size(84, 21);
            this.rbKruznica.TabIndex = 1;
            this.rbKruznica.TabStop = true;
            this.rbKruznica.Text = "Kružnica";
            this.rbKruznica.UseVisualStyleBackColor = true;
            // 
            // rbTocka
            // 
            this.rbTocka.AutoSize = true;
            this.rbTocka.Location = new System.Drawing.Point(0, 32);
            this.rbTocka.Name = "rbTocka";
            this.rbTocka.Size = new System.Drawing.Size(68, 21);
            this.rbTocka.TabIndex = 0;
            this.rbTocka.TabStop = true;
            this.rbTocka.Text = "Točka";
            this.rbTocka.UseVisualStyleBackColor = true;
            // 
            // gbPostavkeKista
            // 
            this.gbPostavkeKista.Controls.Add(this.cbColor);
            this.gbPostavkeKista.Controls.Add(this.rbKist3px);
            this.gbPostavkeKista.Controls.Add(this.rbKist2px);
            this.gbPostavkeKista.Controls.Add(this.rbKist1px);
            this.gbPostavkeKista.Location = new System.Drawing.Point(249, 453);
            this.gbPostavkeKista.Name = "gbPostavkeKista";
            this.gbPostavkeKista.Size = new System.Drawing.Size(205, 74);
            this.gbPostavkeKista.TabIndex = 3;
            this.gbPostavkeKista.TabStop = false;
            this.gbPostavkeKista.Text = "Postavke kista";
            // 
            // rbKist3px
            // 
            this.rbKist3px.AutoSize = true;
            this.rbKist3px.Location = new System.Drawing.Point(120, 32);
            this.rbKist3px.Name = "rbKist3px";
            this.rbKist3px.Size = new System.Drawing.Size(51, 21);
            this.rbKist3px.TabIndex = 2;
            this.rbKist3px.TabStop = true;
            this.rbKist3px.Text = "3px";
            this.rbKist3px.UseVisualStyleBackColor = true;
            // 
            // rbKist2px
            // 
            this.rbKist2px.AutoSize = true;
            this.rbKist2px.Location = new System.Drawing.Point(63, 32);
            this.rbKist2px.Name = "rbKist2px";
            this.rbKist2px.Size = new System.Drawing.Size(51, 21);
            this.rbKist2px.TabIndex = 1;
            this.rbKist2px.TabStop = true;
            this.rbKist2px.Text = "2px";
            this.rbKist2px.UseVisualStyleBackColor = true;
            // 
            // rbKist1px
            // 
            this.rbKist1px.AutoSize = true;
            this.rbKist1px.Location = new System.Drawing.Point(6, 32);
            this.rbKist1px.Name = "rbKist1px";
            this.rbKist1px.Size = new System.Drawing.Size(51, 21);
            this.rbKist1px.TabIndex = 0;
            this.rbKist1px.TabStop = true;
            this.rbKist1px.Text = "1px";
            this.rbKist1px.UseVisualStyleBackColor = true;
            // 
            // gbUndoRedo
            // 
            this.gbUndoRedo.Controls.Add(this.btnRedo);
            this.gbUndoRedo.Controls.Add(this.btnUndo);
            this.gbUndoRedo.Location = new System.Drawing.Point(461, 463);
            this.gbUndoRedo.Name = "gbUndoRedo";
            this.gbUndoRedo.Size = new System.Drawing.Size(136, 64);
            this.gbUndoRedo.TabIndex = 4;
            this.gbUndoRedo.TabStop = false;
            // 
            // btnRedo
            // 
            this.btnRedo.Location = new System.Drawing.Point(64, 22);
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(51, 23);
            this.btnRedo.TabIndex = 1;
            this.btnRedo.Text = "Redo";
            this.btnRedo.UseVisualStyleBackColor = true;
            // 
            // btnUndo
            // 
            this.btnUndo.Location = new System.Drawing.Point(7, 22);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(51, 23);
            this.btnUndo.TabIndex = 0;
            this.btnUndo.Text = "Undo";
            this.btnUndo.UseVisualStyleBackColor = true;
            // 
            // cbColor
            // 
            this.cbColor.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbColor.BackColor = System.Drawing.SystemColors.ControlText;
            this.cbColor.Location = new System.Drawing.Point(168, 32);
            this.cbColor.Name = "cbColor";
            this.cbColor.Size = new System.Drawing.Size(31, 27);
            this.cbColor.TabIndex = 3;
            this.cbColor.Text = "paint";
            this.cbColor.UseVisualStyleBackColor = false;
            // 
            // colorDialog
            // 
            this.colorDialog.AnyColor = true;
            this.colorDialog.FullOpen = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 539);
            this.Controls.Add(this.gbUndoRedo);
            this.Controls.Add(this.gbPostavkeKista);
            this.Controls.Add(this.gbKistovi);
            this.Controls.Add(this.lbPovijest);
            this.Controls.Add(this.pbPlatno);
            this.Name = "Form1";
            this.Text = "PaintSharp";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbPlatno)).EndInit();
            this.gbKistovi.ResumeLayout(false);
            this.gbKistovi.PerformLayout();
            this.gbPostavkeKista.ResumeLayout(false);
            this.gbPostavkeKista.PerformLayout();
            this.gbUndoRedo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbPlatno;
        private System.Windows.Forms.ListBox lbPovijest;
        private System.Windows.Forms.GroupBox gbKistovi;
        private System.Windows.Forms.RadioButton rbKvadrat;
        private System.Windows.Forms.RadioButton rbKruznica;
        private System.Windows.Forms.RadioButton rbTocka;
        private System.Windows.Forms.GroupBox gbPostavkeKista;
        private System.Windows.Forms.RadioButton rbKist3px;
        private System.Windows.Forms.RadioButton rbKist2px;
        private System.Windows.Forms.RadioButton rbKist1px;
        private System.Windows.Forms.GroupBox gbUndoRedo;
        private System.Windows.Forms.Button btnRedo;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.CheckBox cbColor;
        public System.Windows.Forms.ColorDialog colorDialog;
    }
}

