﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace paintsharp
{
    class Engine
    {
        public Engine(int sirina,int visina)
        {
            Platno = new Bitmap(sirina, visina);
            PlatnoSirina = sirina;
            PlatnoVisina = visina;
            debljina = 2;
            g = Graphics.FromImage(Platno);
            koordX = 0;
            koordY = 0;
            Olovka = new Pen(Color.Black, debljina);
            Povijest = new List<string>();
        }

        public void NacrtajTocku(int x,int y)
        {
            Graphics g = Graphics.FromImage(Platno);
            g.DrawEllipse(Olovka, x, y, Debljina, Debljina);
            Povijest.Add(string.Format("Dodana tocka na {0},{1}", x, y));
        }

        public void NacrtajKvadrat(int x1,int y1,int x2,int y2)
        {
            Graphics g = Graphics.FromImage(Platno);
            g.DrawRectangle(Olovka, x1, y1, x2, y2);
            Povijest.Add(string.Format("Dodan kvadrat na {0},{1}", x1, y1,x2,y2));
        }

        public void NacrtajKruznicu(int x1, int y1, int x2, int y2)
        {
            Graphics g = Graphics.FromImage(Platno);
            g.DrawRectangle(Olovka, x1, y1, x2, y2);
            Povijest.Add(string.Format("Dodana kruznica na {0},{1}", x1, y1, x2, y2));
        }

        public List<string> Povijest { get; set; }
        public Bitmap Platno { get; set; }
        public int PlatnoSirina { get; set; }
        public int PlatnoVisina { get; set; }
        public int KoordX { get; set; }
        public int KoordY { get; set; }
        public int Boja { get; set; }
        public int Debljina { get; set; }
        public bool Pomicanje { get; set; }
        public Bitmap Slika { get; set; }
        public Pen Olovka { get; set; }
        public Graphics g { get; set; }



        int koordX;
        int koordY;
        int debljina;
        string boja;
        bool pomicanje;
        Bitmap slika;
        Pen olovka;
        Graphics grafika;
        List<string> povijest;

    }
}

//brine se za sve dogadzaje na platnu
