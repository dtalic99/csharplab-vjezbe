﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace paintsharp
{
    public partial class Form1 : Form
    {
        Engine engine;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            engine = new Engine(pbPlatno.Width, pbPlatno.Height);
        }

        private void cbColor_CheckedChanged(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                prikazi("Odabrali ste: " + colorDialog.Color.Name);
                engine.Olovka.Color = colorDialog.Color;
            }
        }

        private void pbPlatno_MouseDown(object sender, MouseEventArgs e)
        {
            engine.Pomicanje = true;
            engine.KoordX = e.X;
            engine.KoordY = e.Y;
            if (rbTocka.Checked)
            {
                engine.NacrtajTocku(e.X, e.Y);
            }
            OsvjeziPlatno();
        }

        private void OsvjeziPlatno()
        {
            pbPlatno.Image = engine.Platno;
            lbPovijest.Items.Clear();
            lbPovijest.Items.AddRange(engine.Povijest.ToArray());
        }

        private void pbPlatno_MouseUp(object sender, MouseEventArgs e)
        {
            engine.Pomicanje = false;
            engine.KoordX = e.X;
            engine.KoordY = e.Y;

            Rectangle shape = new Rectangle(engine.KoordX, engine.KoordY, engine.PlatnoVisina, engine.PlatnoSirina);

            if (rbKvadrat.Checked)
            {
                engine.NacrtajKvadrat(engine.KoordX, engine.KoordY, engine.PlatnoVisina, engine.PlatnoSirina);
            }

            if (rbKruznica.Checked)
            {
                engine.NacrtajKruznicu(engine.KoordX, engine.KoordY, engine.PlatnoVisina, engine.PlatnoSirina);
            }

            OsvjeziPlatno();
        }

        private void rbKist1px_CheckedChanged(object sender, EventArgs e)
        {
            rbKist2px.Checked = false;
            rbKist3px.Checked = false;
            engine.Olovka.Width = 1;
        }

        private void rbKist2px_CheckedChanged(object sender, EventArgs e)
        {
            rbKist1px.Checked = false;
            rbKist3px.Checked = false;
            engine.Olovka.Width = 2;
        }

        private void rbKist3px_CheckedChanged(object sender, EventArgs e)
        {
            rbKist1px.Checked = false;
            rbKist2px.Checked = false;
            engine.Olovka.Width = 3;
        }

        private void rbTocka_CheckedChanged(object sender, EventArgs e)
        {
            rbKvadrat.Checked = false;
            rbKruznica.Checked = false;
        }

        private void rbKruznica_CheckedChanged(object sender, EventArgs e)
        {
            rbTocka.Checked = false;
            rbKvadrat.Checked = false;
        }

        private void rbKvadrat_CheckedChanged(object sender, EventArgs e)
        {
            rbTocka.Checked = false;
            rbKruznica.Checked = false;
        }

        private void prikazi(string text)
        {
            MessageBox.Show(text);
        }
    }
}
