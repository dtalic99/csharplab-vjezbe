﻿
namespace NotesApp
{
    partial class Notes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNotes = new System.Windows.Forms.ListBox();
            this.tbNewNote = new System.Windows.Forms.TextBox();
            this.btnNewNote = new System.Windows.Forms.Button();
            this.tbNote = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbNotes
            // 
            this.lbNotes.FormattingEnabled = true;
            this.lbNotes.ItemHeight = 16;
            this.lbNotes.Location = new System.Drawing.Point(12, 12);
            this.lbNotes.Name = "lbNotes";
            this.lbNotes.Size = new System.Drawing.Size(155, 372);
            this.lbNotes.TabIndex = 0;
            this.lbNotes.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lbNotes_MouseDoubleClick);
            // 
            // tbNewNote
            // 
            this.tbNewNote.Location = new System.Drawing.Point(13, 391);
            this.tbNewNote.Name = "tbNewNote";
            this.tbNewNote.Size = new System.Drawing.Size(154, 22);
            this.tbNewNote.TabIndex = 1;
            // 
            // btnNewNote
            // 
            this.btnNewNote.Location = new System.Drawing.Point(12, 420);
            this.btnNewNote.Name = "btnNewNote";
            this.btnNewNote.Size = new System.Drawing.Size(155, 23);
            this.btnNewNote.TabIndex = 2;
            this.btnNewNote.Text = "Nova Bilješka";
            this.btnNewNote.UseVisualStyleBackColor = true;
            this.btnNewNote.Click += new System.EventHandler(this.btnNewNote_Click);
            // 
            // tbNote
            // 
            this.tbNote.Location = new System.Drawing.Point(194, 12);
            this.tbNote.Multiline = true;
            this.tbNote.Name = "tbNote";
            this.tbNote.Size = new System.Drawing.Size(579, 401);
            this.tbNote.TabIndex = 3;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(618, 419);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(155, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Pohrani";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // Notes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbNote);
            this.Controls.Add(this.btnNewNote);
            this.Controls.Add(this.tbNewNote);
            this.Controls.Add(this.lbNotes);
            this.Name = "Notes";
            this.Text = "NotesApp";
            this.Load += new System.EventHandler(this.Notes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbNotes;
        private System.Windows.Forms.TextBox tbNewNote;
        private System.Windows.Forms.Button btnNewNote;
        private System.Windows.Forms.TextBox tbNote;
        private System.Windows.Forms.Button btnSave;
    }
}

