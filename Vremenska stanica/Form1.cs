﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vremenska_stanica
{
    public partial class VremenskaStanica : Form
    {
        Vrijeme vrijeme;
        public VremenskaStanica()
        {
            InitializeComponent();
            vrijeme = new Vrijeme();
        }

        private void VremenskaStanica_Load(object sender, EventArgs e)
        {
            foreach (var grad in vrijeme.DohvatiGradove())
            {
                cmbListaGradova.Items.Add(grad);
                lbTopliGradovi.Items.AddRange(vrijeme.DohvatiNajtoplije().ToArray());
                lbHladniGradovi.Items.AddRange(vrijeme.DohvatiNajhladnije().ToArray());
            }
        }

        private void cmbListaGradova_TextChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(cmbListaGradova.Text);
            var podaci = vrijeme.DohvatiPodatkeZaGrad(cmbListaGradova.Text);
            OsvjeziPrikaz();
        }

        private void tmrOsvjezi_Tick(object sender, EventArgs e)
        {
            vrijeme.Osvjezi();
            OsvjeziPrikaz();
        }

        public void PrikaziVrijeme()
        {
            DateTime now = DateTime.Now;
            statusStrip2.Text = "Zadnji put osvježeno: " + now.ToString();
        }

        private void OsvjeziPrikaz()
        {
            lbHladniGradovi.Items.Clear();
            lbTopliGradovi.Items.Clear();
            lbTopliGradovi.Items.AddRange(vrijeme.DohvatiNajtoplije().ToArray());
            lbHladniGradovi.Items.AddRange(vrijeme.DohvatiNajhladnije().ToArray())
            if (cmbListaGradova.Text != "")
             {
                var podaci = vrijeme.DohvatiPodatkeZaGrad(cmbListaGradova.Text);
                lblGrad.Text = podaci.Grad;
                lblTemperatura.Text = podaci.Temperatura.ToString() + "°C";
                lblVlaga.Text = podaci.Vlaga.ToString() + " %";
                lblTlak.Text = podaci.Tlak.ToString() + " hPa";
            }
        }
    }
}
