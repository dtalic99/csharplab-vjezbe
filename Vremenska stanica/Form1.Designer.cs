﻿
namespace Vremenska_stanica
{
    partial class VremenskaStanica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbListaGradova = new System.Windows.Forms.ComboBox();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblTemperatura = new System.Windows.Forms.Label();
            this.lblVlaga = new System.Windows.Forms.Label();
            this.lblTlak = new System.Windows.Forms.Label();
            this.lbTopliGradovi = new System.Windows.Forms.ListBox();
            this.lbHladniGradovi = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblOsvjezeno = new System.Windows.Forms.Label();
            this.tmrOsvjezi = new System.Windows.Forms.Timer(this.components);
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbListaGradova
            // 
            this.cmbListaGradova.FormattingEnabled = true;
            this.cmbListaGradova.Location = new System.Drawing.Point(12, 12);
            this.cmbListaGradova.Name = "cmbListaGradova";
            this.cmbListaGradova.Size = new System.Drawing.Size(425, 24);
            this.cmbListaGradova.TabIndex = 0;
            this.cmbListaGradova.Text = "Odaberite grad";
            this.cmbListaGradova.TextChanged += new System.EventHandler(this.cmbListaGradova_TextChanged);
            // 
            // lblGrad
            // 
            this.lblGrad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblGrad.Location = new System.Drawing.Point(156, 86);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(100, 23);
            this.lblGrad.TabIndex = 1;
            this.lblGrad.Text = "-";
            this.lblGrad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTemperatura
            // 
            this.lblTemperatura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTemperatura.Location = new System.Drawing.Point(156, 145);
            this.lblTemperatura.Name = "lblTemperatura";
            this.lblTemperatura.Size = new System.Drawing.Size(100, 23);
            this.lblTemperatura.TabIndex = 2;
            this.lblTemperatura.Text = "- °C";
            this.lblTemperatura.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVlaga
            // 
            this.lblVlaga.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblVlaga.Location = new System.Drawing.Point(156, 203);
            this.lblVlaga.Name = "lblVlaga";
            this.lblVlaga.Size = new System.Drawing.Size(100, 23);
            this.lblVlaga.TabIndex = 3;
            this.lblVlaga.Text = "- %";
            this.lblVlaga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTlak
            // 
            this.lblTlak.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblTlak.Location = new System.Drawing.Point(156, 257);
            this.lblTlak.Name = "lblTlak";
            this.lblTlak.Size = new System.Drawing.Size(100, 23);
            this.lblTlak.TabIndex = 4;
            this.lblTlak.Text = "- hPa";
            this.lblTlak.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTopliGradovi
            // 
            this.lbTopliGradovi.FormattingEnabled = true;
            this.lbTopliGradovi.ItemHeight = 16;
            this.lbTopliGradovi.Location = new System.Drawing.Point(12, 320);
            this.lbTopliGradovi.Name = "lbTopliGradovi";
            this.lbTopliGradovi.Size = new System.Drawing.Size(170, 148);
            this.lbTopliGradovi.TabIndex = 5;
            // 
            // lbHladniGradovi
            // 
            this.lbHladniGradovi.FormattingEnabled = true;
            this.lbHladniGradovi.ItemHeight = 16;
            this.lbHladniGradovi.Location = new System.Drawing.Point(267, 320);
            this.lbHladniGradovi.Name = "lbHladniGradovi";
            this.lbHladniGradovi.Size = new System.Drawing.Size(170, 148);
            this.lbHladniGradovi.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 300);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Najtopliji gradovi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(287, 300);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Najhladniji gradovi:";
            // 
            // lblOsvjezeno
            // 
            this.lblOsvjezeno.AutoSize = true;
            this.lblOsvjezeno.Location = new System.Drawing.Point(12, 484);
            this.lblOsvjezeno.Name = "lblOsvjezeno";
            this.lblOsvjezeno.Size = new System.Drawing.Size(139, 17);
            this.lblOsvjezeno.TabIndex = 9;
            this.lblOsvjezeno.Text = "Zadnji put osvježeno";
            // 
            // tmrOsvjezi
            // 
            this.tmrOsvjezi.Interval = 300000;
            this.tmrOsvjezi.Tick += new System.EventHandler(this.tmrOsvjezi_Tick);
            // 
            // statusStrip2
            // 
            this.statusStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip2.Location = new System.Drawing.Point(0, 484);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(449, 26);
            this.statusStrip2.TabIndex = 11;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(150, 20);
            this.toolStripStatusLabel1.Text = "Zadnji put osvjezeno:";
            // 
            // VremenskaStanica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 510);
            this.Controls.Add(this.statusStrip2);
            this.Controls.Add(this.lblOsvjezeno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbHladniGradovi);
            this.Controls.Add(this.lbTopliGradovi);
            this.Controls.Add(this.lblTlak);
            this.Controls.Add(this.lblVlaga);
            this.Controls.Add(this.lblTemperatura);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.cmbListaGradova);
            this.Name = "VremenskaStanica";
            this.Text = "Vrijeme";
            this.Load += new System.EventHandler(this.VremenskaStanica_Load);
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbListaGradova;
        private System.Windows.Forms.Label lblGrad;
        private System.Windows.Forms.Label lblTemperatura;
        private System.Windows.Forms.Label lblVlaga;
        private System.Windows.Forms.Label lblTlak;
        private System.Windows.Forms.ListBox lbTopliGradovi;
        private System.Windows.Forms.ListBox lbHladniGradovi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblOsvjezeno;
        private System.Windows.Forms.Timer tmrOsvjezi;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    }
}

