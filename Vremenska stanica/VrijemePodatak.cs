﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vremenska_stanica
{
    class VrijemePodatak
    {
        public VrijemePodatak(string grad, double temperatura, double vlaga, double tlak)
        {
            Grad = grad;
            Temperatura = temperatura;
            Vlaga = vlaga;
            Tlak = tlak;
        }
        public string Grad { get; set; }
        public double Temperatura { get; set; }
        public double Vlaga { get; set; }
        public double Tlak { get; set; }

    }
}
