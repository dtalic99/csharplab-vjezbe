﻿
namespace Pomodoro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblRad = new System.Windows.Forms.Label();
            this.tbRad = new System.Windows.Forms.TextBox();
            this.tbOdmor = new System.Windows.Forms.TextBox();
            this.lblOdmor = new System.Windows.Forms.Label();
            this.lblVrijeme = new System.Windows.Forms.Label();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.tmrOdbrojavanje = new System.Windows.Forms.Timer(this.components);
            this.lblPrikazRad = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRad
            // 
            this.lblRad.AutoSize = true;
            this.lblRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblRad.Location = new System.Drawing.Point(67, 35);
            this.lblRad.Name = "lblRad";
            this.lblRad.Size = new System.Drawing.Size(47, 25);
            this.lblRad.TabIndex = 0;
            this.lblRad.Text = "Rad";
            // 
            // tbRad
            // 
            this.tbRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbRad.Location = new System.Drawing.Point(47, 63);
            this.tbRad.Name = "tbRad";
            this.tbRad.Size = new System.Drawing.Size(100, 30);
            this.tbRad.TabIndex = 2;
            this.tbRad.Text = "25";
            // 
            // tbOdmor
            // 
            this.tbOdmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbOdmor.Location = new System.Drawing.Point(615, 63);
            this.tbOdmor.Name = "tbOdmor";
            this.tbOdmor.Size = new System.Drawing.Size(100, 30);
            this.tbOdmor.TabIndex = 4;
            this.tbOdmor.Text = "5";
            this.tbOdmor.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblOdmor
            // 
            this.lblOdmor.AutoSize = true;
            this.lblOdmor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOdmor.Location = new System.Drawing.Point(635, 35);
            this.lblOdmor.Name = "lblOdmor";
            this.lblOdmor.Size = new System.Drawing.Size(72, 25);
            this.lblOdmor.TabIndex = 3;
            this.lblOdmor.Text = "Odmor";
            this.lblOdmor.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblVrijeme
            // 
            this.lblVrijeme.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblVrijeme.Location = new System.Drawing.Point(47, 146);
            this.lblVrijeme.Name = "lblVrijeme";
            this.lblVrijeme.Size = new System.Drawing.Size(668, 48);
            this.lblVrijeme.TabIndex = 5;
            this.lblVrijeme.Text = "25:00";
            this.lblVrijeme.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnStartStop
            // 
            this.btnStartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnStartStop.Location = new System.Drawing.Point(47, 300);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(122, 47);
            this.btnStartStop.TabIndex = 6;
            this.btnStartStop.Text = "Start/Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // btnReset
            // 
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnReset.Location = new System.Drawing.Point(593, 300);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(122, 47);
            this.btnReset.TabIndex = 7;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // tmrOdbrojavanje
            // 
            this.tmrOdbrojavanje.Interval = 50;
            this.tmrOdbrojavanje.Tick += new System.EventHandler(this.tmrOdbrojavanje_Tick);
            // 
            // lblPrikazRad
            // 
            this.lblPrikazRad.AutoSize = true;
            this.lblPrikazRad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPrikazRad.Location = new System.Drawing.Point(350, 109);
            this.lblPrikazRad.Name = "lblPrikazRad";
            this.lblPrikazRad.Size = new System.Drawing.Size(53, 25);
            this.lblPrikazRad.TabIndex = 8;
            this.lblPrikazRad.Text = "RAD";
            this.lblPrikazRad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 458);
            this.Controls.Add(this.lblPrikazRad);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.lblVrijeme);
            this.Controls.Add(this.tbOdmor);
            this.Controls.Add(this.lblOdmor);
            this.Controls.Add(this.tbRad);
            this.Controls.Add(this.lblRad);
            this.Name = "Form1";
            this.Text = "Pomodoro";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRad;
        private System.Windows.Forms.TextBox tbRad;
        private System.Windows.Forms.TextBox tbOdmor;
        private System.Windows.Forms.Label lblOdmor;
        private System.Windows.Forms.Label lblVrijeme;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Timer tmrOdbrojavanje;
        private System.Windows.Forms.Label lblPrikazRad;
    }
}

