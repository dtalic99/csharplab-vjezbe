﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pomodoro
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            radPokrenut = true;
            AzurirajBrojSekundi(tbRad.Text);
            AzurirajPrikaz();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            /* if (tmrOdbrojavanje.Enabled)
             {
                 tmrOdbrojavanje.Enabled = false;
             }
             else
             {
                 tmrOdbrojavanje.Enabled = true;
             }*/
            tmrOdbrojavanje.Enabled = !tmrOdbrojavanje.Enabled;

        }

        private void tmrOdbrojavanje_Tick(object sender, EventArgs e)
        {
            if (trenutanBrojSkundi == 0)
            {
                if (radPokrenut)
                {
                    AzurirajBrojSekundi(tbOdmor.Text);
                    radPokrenut = false;
                }
                else
                {
                    AzurirajBrojSekundi(tbRad.Text);
                    radPokrenut = true;
                }
            }
            trenutanBrojSkundi--;

            //lblVrijeme.Text = trenutanBrojSkundi.ToString();
            //lblVrijeme.Text = (trenutanBrojSkundi / 60).ToString() + ":" + (trenutanBrojSkundi % 60).ToString();
            AzurirajPrikaz();
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            tmrOdbrojavanje.Enabled = false;
            AzurirajBrojSekundi(tbRad.Text);
            AzurirajPrikaz();
        }

        private void AzurirajBrojSekundi(string brojMinuta)
        {
            trenutanBrojSkundi = int.Parse(brojMinuta) * 60;
        }
        private void AzurirajPrikaz()
        {
            if (radPokrenut)
            {
                lblPrikazRad.Text = "RAD";
            }
            else
            {
                lblPrikazRad.Text = "ODMOR";
            }
            lblVrijeme.Text = string.Format("{0:D2}:{1:D2}", trenutanBrojSkundi / 60, trenutanBrojSkundi % 60);
        }
        private int trenutanBrojSkundi;
        private bool radPokrenut;
    }
}
