﻿using NotesAppEF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesAppEF.Data.Context
{
    public class NotesDbContext : DbContext
    {
        public NotesDbContext() : base("name=NoteItems")
        {
            this.Configuration.LazyLoadingEnabled = false;

        }
        public virtual DbSet<NoteItem> NotesItems { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
