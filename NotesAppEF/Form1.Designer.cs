﻿
namespace NotesAppEF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNotes = new System.Windows.Forms.ListBox();
            this.tbNewNote = new System.Windows.Forms.TextBox();
            this.tbNote = new System.Windows.Forms.TextBox();
            this.btnNewNote = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbNotes
            // 
            this.lbNotes.FormattingEnabled = true;
            this.lbNotes.ItemHeight = 16;
            this.lbNotes.Location = new System.Drawing.Point(0, 0);
            this.lbNotes.Name = "lbNotes";
            this.lbNotes.Size = new System.Drawing.Size(195, 372);
            this.lbNotes.TabIndex = 0;
            // 
            // tbNewNote
            // 
            this.tbNewNote.Location = new System.Drawing.Point(0, 378);
            this.tbNewNote.Name = "tbNewNote";
            this.tbNewNote.Size = new System.Drawing.Size(195, 22);
            this.tbNewNote.TabIndex = 1;
            // 
            // tbNote
            // 
            this.tbNote.Location = new System.Drawing.Point(201, 0);
            this.tbNote.Multiline = true;
            this.tbNote.Name = "tbNote";
            this.tbNote.Size = new System.Drawing.Size(587, 400);
            this.tbNote.TabIndex = 2;
            // 
            // btnNewNote
            // 
            this.btnNewNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnNewNote.Location = new System.Drawing.Point(0, 406);
            this.btnNewNote.Name = "btnNewNote";
            this.btnNewNote.Size = new System.Drawing.Size(195, 32);
            this.btnNewNote.TabIndex = 3;
            this.btnNewNote.Text = "Nova Bilješka";
            this.btnNewNote.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnSave.Location = new System.Drawing.Point(593, 406);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(195, 32);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Pohrani";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnNewNote);
            this.Controls.Add(this.tbNote);
            this.Controls.Add(this.tbNewNote);
            this.Controls.Add(this.lbNotes);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbNotes;
        private System.Windows.Forms.TextBox tbNewNote;
        private System.Windows.Forms.TextBox tbNote;
        private System.Windows.Forms.Button btnNewNote;
        private System.Windows.Forms.Button btnSave;
    }
}

