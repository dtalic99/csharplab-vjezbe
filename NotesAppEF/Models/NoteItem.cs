﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotesAppEF.Models
{
    public class NoteItem
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Name { get; set; }
    }
}
