﻿
namespace Oglasnik
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNaslov = new System.Windows.Forms.Label();
            this.tbNaslov = new System.Windows.Forms.TextBox();
            this.lbTekst = new System.Windows.Forms.Label();
            this.tbTekst = new System.Windows.Forms.TextBox();
            this.cbSlikovniOglas = new System.Windows.Forms.CheckBox();
            this.cbVideoOglas = new System.Windows.Forms.CheckBox();
            this.tbSlikovniLink = new System.Windows.Forms.TextBox();
            this.tbVideoLink = new System.Windows.Forms.TextBox();
            this.btnDodajOglas = new System.Windows.Forms.Button();
            this.lblOglasi = new System.Windows.Forms.Label();
            this.lbOglasi = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbNaslov
            // 
            this.lbNaslov.AutoSize = true;
            this.lbNaslov.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbNaslov.Location = new System.Drawing.Point(12, 9);
            this.lbNaslov.Name = "lbNaslov";
            this.lbNaslov.Size = new System.Drawing.Size(60, 20);
            this.lbNaslov.TabIndex = 0;
            this.lbNaslov.Text = "Naslov";
            // 
            // tbNaslov
            // 
            this.tbNaslov.Location = new System.Drawing.Point(16, 33);
            this.tbNaslov.Name = "tbNaslov";
            this.tbNaslov.Size = new System.Drawing.Size(193, 22);
            this.tbNaslov.TabIndex = 1;
            // 
            // lbTekst
            // 
            this.lbTekst.AutoSize = true;
            this.lbTekst.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbTekst.Location = new System.Drawing.Point(13, 70);
            this.lbTekst.Name = "lbTekst";
            this.lbTekst.Size = new System.Drawing.Size(50, 20);
            this.lbTekst.TabIndex = 2;
            this.lbTekst.Text = "Tekst";
            // 
            // tbTekst
            // 
            this.tbTekst.Location = new System.Drawing.Point(16, 93);
            this.tbTekst.Multiline = true;
            this.tbTekst.Name = "tbTekst";
            this.tbTekst.Size = new System.Drawing.Size(192, 218);
            this.tbTekst.TabIndex = 3;
            // 
            // cbSlikovniOglas
            // 
            this.cbSlikovniOglas.AutoSize = true;
            this.cbSlikovniOglas.Location = new System.Drawing.Point(16, 326);
            this.cbSlikovniOglas.Name = "cbSlikovniOglas";
            this.cbSlikovniOglas.Size = new System.Drawing.Size(119, 21);
            this.cbSlikovniOglas.TabIndex = 4;
            this.cbSlikovniOglas.Text = "Slikovni Oglas";
            this.cbSlikovniOglas.UseVisualStyleBackColor = true;
            // 
            // cbVideoOglas
            // 
            this.cbVideoOglas.AutoSize = true;
            this.cbVideoOglas.Location = new System.Drawing.Point(17, 397);
            this.cbVideoOglas.Name = "cbVideoOglas";
            this.cbVideoOglas.Size = new System.Drawing.Size(107, 21);
            this.cbVideoOglas.TabIndex = 5;
            this.cbVideoOglas.Text = "Video Oglas";
            this.cbVideoOglas.UseVisualStyleBackColor = true;
            // 
            // tbSlikovniLink
            // 
            this.tbSlikovniLink.Location = new System.Drawing.Point(12, 353);
            this.tbSlikovniLink.Name = "tbSlikovniLink";
            this.tbSlikovniLink.Size = new System.Drawing.Size(193, 22);
            this.tbSlikovniLink.TabIndex = 6;
            // 
            // tbVideoLink
            // 
            this.tbVideoLink.Location = new System.Drawing.Point(12, 424);
            this.tbVideoLink.Name = "tbVideoLink";
            this.tbVideoLink.Size = new System.Drawing.Size(193, 22);
            this.tbVideoLink.TabIndex = 7;
            // 
            // btnDodajOglas
            // 
            this.btnDodajOglas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnDodajOglas.Location = new System.Drawing.Point(16, 486);
            this.btnDodajOglas.Name = "btnDodajOglas";
            this.btnDodajOglas.Size = new System.Drawing.Size(189, 47);
            this.btnDodajOglas.TabIndex = 8;
            this.btnDodajOglas.Text = "Dodaj Oglas";
            this.btnDodajOglas.UseVisualStyleBackColor = true;
            this.btnDodajOglas.Click += new System.EventHandler(this.btnDodajOglas_Click);
            // 
            // lblOglasi
            // 
            this.lblOglasi.AutoSize = true;
            this.lblOglasi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblOglasi.Location = new System.Drawing.Point(251, 9);
            this.lblOglasi.Name = "lblOglasi";
            this.lblOglasi.Size = new System.Drawing.Size(57, 20);
            this.lblOglasi.TabIndex = 9;
            this.lblOglasi.Text = "Oglasi";
            // 
            // lbOglasi
            // 
            this.lbOglasi.FormattingEnabled = true;
            this.lbOglasi.ItemHeight = 16;
            this.lbOglasi.Location = new System.Drawing.Point(255, 33);
            this.lbOglasi.Name = "lbOglasi";
            this.lbOglasi.Size = new System.Drawing.Size(392, 500);
            this.lbOglasi.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 553);
            this.Controls.Add(this.lbOglasi);
            this.Controls.Add(this.lblOglasi);
            this.Controls.Add(this.btnDodajOglas);
            this.Controls.Add(this.tbVideoLink);
            this.Controls.Add(this.tbSlikovniLink);
            this.Controls.Add(this.cbVideoOglas);
            this.Controls.Add(this.cbSlikovniOglas);
            this.Controls.Add(this.tbTekst);
            this.Controls.Add(this.lbTekst);
            this.Controls.Add(this.tbNaslov);
            this.Controls.Add(this.lbNaslov);
            this.Name = "Form1";
            this.Text = "Oglasnik";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbNaslov;
        private System.Windows.Forms.TextBox tbNaslov;
        private System.Windows.Forms.Label lbTekst;
        private System.Windows.Forms.TextBox tbTekst;
        private System.Windows.Forms.CheckBox cbSlikovniOglas;
        private System.Windows.Forms.CheckBox cbVideoOglas;
        private System.Windows.Forms.TextBox tbSlikovniLink;
        private System.Windows.Forms.TextBox tbVideoLink;
        private System.Windows.Forms.Button btnDodajOglas;
        private System.Windows.Forms.Label lblOglasi;
        private System.Windows.Forms.ListBox lbOglasi;
    }
}

